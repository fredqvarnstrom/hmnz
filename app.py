#! /usr/bin/env python
import Tkinter as Tk
from Tkinter import Frame
from functools import partial
from widgets.adder import Adder
from widgets.base_words import BaseFiles
from widgets.mutator import Mutator
from tkFileDialog import asksaveasfile
from tkMessageBox import askyesno
from widgets.scrollable_frame import VerticalScrolledFrame


class TMain(Frame):
    """
    Main widget
    """

    def __init__(self, master):
        Frame.__init__(self, master)
        self.nodes = []
        self.base_file_box = None

        # Upper status bar
        self.upper_status_bar = Tk.Frame(self)
        self.lb_total_words = Tk.Label(self.upper_status_bar, text="Total Words: 93,032", font=("Helvetica", "12"))
        self.lb_total_words.pack(side="left", padx=10, pady=10)
        self.lb_est_opt_size = Tk.Label(self.upper_status_bar, text="Est. Output Size: 42GB", font=("Helvetica", "12"))
        self.lb_est_opt_size.pack(side="left", padx=10, pady=10)
        mb = Tk.Menubutton(self.upper_status_bar, text=" + ", relief=Tk.RAISED, font=("Helvetica", "14"))
        mb.menu = Tk.Menu(mb, tearoff=0)
        mb["menu"] = mb.menu
        mb.menu.add_command(label="Mutator", command=partial(self.add_block, 'mutate'))
        mb.menu.add_command(label="Prepend", command=partial(self.add_block, 'Prepend'))
        mb.menu.add_command(label="Append", command=partial(self.add_block, 'Append'))
        mb.pack(side="right", fill="both", padx=10, pady=5)
        self.btn_process = Tk.Button(self.upper_status_bar, text='Process')
        self.btn_process.pack(fill="both", side="right", padx=10, pady=5)
        self.btn_process.bind("<ButtonRelease-1>", self.on_btn_process)
        self.upper_status_bar.pack(side="top", fill="both")

        self.scr_box = VerticalScrolledFrame(self)
        self.scr_box.pack(fill='both', side='top', expand=True)

        self.base_file_box = BaseFiles(self.scr_box.interior, self)
        self.nodes.append(self.base_file_box)

        self.pack(fill="both", padx=10, pady=10, expand=True)

    def on_btn_process(self, event):
        """
        :param event:
        :return:
        """
        if self.btn_process['state'] == 'disabled':
            return
        opt_file_path = asksaveasfile()
        if opt_file_path:
            result = []
            for node in self.nodes:
                result.append({
                    'type': node.title,
                    'values': node.get_values()
                })
            import pprint
            pprint.pprint(result)
            print 'Output File: {}'.format(opt_file_path.name)

    def add_block(self, _type, *args):
        """
        Add new node(Mutator, Append, Prepend)
        :param _type:
        :param args:
        :return:
        """
        if _type == 'mutate':
            item = Mutator(self.scr_box.interior, main=self, number=len(self.nodes) + 1)
        else:
            item = Adder(self.scr_box.interior, main=self, _type=_type, number=len(self.nodes) + 1)
        self.nodes.append(item)

    def on_remove_block(self, node, *args):
        answer = askyesno("Remove Node", "Are you sure you want to delete this node?", icon='warning')
        if answer:
            node.destroy()
            self.nodes.remove(node)
            self.sort_numbers()

    def move_node(self, node, direction, *args):
        """
        Callback when arrow button of a node is pushed.
        :param node:
        :param direction: `up` or `down`
        :param args:
        :return:
        """
        index = self.nodes.index(node)
        if direction == 'up' and index == 1:
            return
        if direction == 'down' and index == len(self.nodes) - 1:
            return

        # Exchange the node with the previous(or the next) node, and sort all nodes again
        if direction == 'up':
            sub_list = [node, self.nodes[index - 1]] + self.nodes[index + 1:]
            new_index = index - 1
        else:
            sub_list = [self.nodes[index + 1], node] + self.nodes[index + 2:]
            new_index = index
        for s in sub_list:
            s.pack_forget()
        for s in sub_list:
            s.pack(fill='both', expand=True, side='top')
        self.nodes[new_index:] = sub_list
        self.sort_numbers()

    def sort_numbers(self):
        """
        Sort numbers of each node
        :return:
        """
        for i, node in enumerate(self.nodes):
            node.update_number(i + 1)

# ============================= Call these functions after interacting with backend  =============================
    def update_total_words(self, words):
        """
        Update `Total Words` label with given value
        :param words: Words count
        :return:
        """
        self.lb_total_words.configure(text='Total Words: {}'.format(words))

    def update_est_opt_size(self, size):
        """
        Update `Est Opt Size` label
        :param size: File size in Gigabyte
        :return:
        """
        self.lb_est_opt_size.configure(text='Est. Output Size: {}GB'.format(size))


class HmnzApp(Tk.Frame):

    def __init__(self, master=None, width=900, height=800):
        Tk.Frame.__init__(self, master)
        self.master.title('HMNZ')
        self.master.resizable(width=False, height=False)
        self.master.geometry('{}x{}'.format(width, height))
        self.pack(padx=0, pady=0, fill=Tk.BOTH, expand=1)
        TMain(self)


if __name__ == '__main__':
    app = HmnzApp()
    app.mainloop()
